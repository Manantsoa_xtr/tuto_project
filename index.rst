.. Tuto project documentation master file, created by
   sphinx-quickstart on Sat May 19 12:25:55 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _project_tuto:

================
Project tutorial
================


.. only:: html

    .. sidebar:: Project tutorial

        :Release: |release|
        :Date: |today|
        :Authors: **A devops guy**

.. seealso::

   - https://gdevops.gitlab.io/tuto_project
   - https://gitlab.com/gdevops/tuto_project
   - https://gdevops.gitlab.io/tuto_git
   - https://github.com/coreinfrastructure/best-practices-badge
   - :ref:`genindex`
   - :ref:`search`



.. toctree::
   :maxdepth: 4


   culture/culture
   project_management/project_management
   news/news
   glossary/glossary
