

.. index::
   Projects ; Culture

.. _culture:

====================
Culture
====================

.. contents::
   :depth: 3


Culture 1 : The smartest people I know: 1. Admit they know very little
========================================================================

The smartest people I know:

1. Admit they know very little 
2. Constantly seek more info 
3. Encourage intellectual debate 
4. Have strong opinions, loosely held 
5. Are comfortable being wrong 
6. Surrounded by best people 
7. Realize life isn't black & white 
8. Fear bias & arrogance


Culture 2 : 15 habits of lucky people: 1. work hard 
=====================================================


15 habits of lucky people: 

1. work hard 
2. complain less 
3. teach others 
4. show gratitude 
5. share credit 
6. choose kindness 
7. volunteer first 
8. unselfishly give 
9. trust first 
10. love customers 
11. stay teachable 
12. promote others 
13. love to explore 
14. storytellers 
15. love to compete




