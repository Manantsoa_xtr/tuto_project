.. index::
   pair: 10.6 ; Gitlab

.. _gitlab_10_6:

==========================
Gitlab 10.6 (2018-03-22)
==========================

.. seealso::

   - https://about.gitlab.com/2018/03/22/gitlab-10-6-released/
   - https://about.gitlab.com/2018/03/22/gitlab-10-6-released/#gitlab-cicd-for-external-repos


.. contents::
   :depth: 3
   

Introducing GitLab CI/CD for GitHub
======================================

.. seealso::

   - https://about.gitlab.com/2018/03/22/gitlab-10-6-released/#gitlab-cicd-for-external-repos



Kubernetes on GitLab keeps getting better
==========================================

With this release, we make it even easier for users to use Kubernetes 
with GitLab. 
You can now deploy a GitLab Runner to your connected Kubernetes cluster 
with a single click
