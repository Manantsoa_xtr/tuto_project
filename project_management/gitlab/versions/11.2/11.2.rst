.. index::
   pair: 11.2 ; Gitlab

.. _gitlab_11_2:

===============================================================================================
Gitlab 11.2 released with live preview in the Web IDE and Android project import (2018-08-22)
===============================================================================================

.. seealso::

   - https://about.gitlab.com/2018/08/22/gitlab-11-2-released/
