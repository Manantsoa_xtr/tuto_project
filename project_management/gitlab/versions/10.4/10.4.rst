.. index::
   pair: 10.4 ; Gitlab

.. _gitlab_10_4:

==========================
Gitlab 10.4 (2018-01-22)
==========================

.. seealso::

   - https://about.gitlab.com/2018/01/22/gitlab-10-4-released/
   - https://about.gitlab.com/2018/01/22/gitlab-10-4-released/#sast-for-docker-containers


.. contents::
   :depth: 3
   
   

Introduction
============

In this month's release of GitLab 10.4 – the first of 2018 – we've added 
capabilities to improve planning, testing, merge requests, and deployment. 
This release also includes new security testing capabilities and the 
first iteration of our Web IDE, part of our ambitious product vision 
for Complete DevOps.

Security testing
=================

.. seealso::

   - https://about.gitlab.com/2018/01/22/gitlab-10-4-released/#sast-for-docker-containers
   

As part of Complete Devops, we want to offer powerful security tools out 
of the box. We recently released static application security testing 
and are now expanding that with Dynamic Application Security Testing (DAST) 
and Static Application Security Testing (SAST) for Docker Containers. 

DAST, SAST for Docker Containers, and Browser Performance Testing have 
also been added as a best practice to Auto DevOps.

Modern applications that run inside of Containers are more secure 
because the code is separated from other Containers on the same host. 
But even if your code is safe, the environment it runs on may contain 
flaws that can impact the security of the entire application, 
for example, a vulnerable system library.

GitLab 10.4 includes the ability to run security checks on the image 
that contains your application and to report possible warnings in the 
Merge Request before merging the changes into the stable branch. 
These checks are part of Auto DevOps to provide security by default.


