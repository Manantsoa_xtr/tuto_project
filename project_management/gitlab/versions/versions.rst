.. index::
   pair: Versions ; Gitlab

.. _gitlab_versions:

====================
Gitlab versions
====================

.. seealso::

   - https://about.gitlab.com/releases/
   - https://about.gitlab.com/direction
   - https://about.gitlab.com/direction/#future-releases
   - https://about.gitlab.com/blog/categories/releases/

.. toctree::
   :maxdepth: 3

   11.3/11.3
   11.2/11.2
   11.1/11.1
   10.8/10.8
   10.7/10.7
   10.6/10.6
   10.5/10.5
   10.4/10.4
   10.3/10.3
   10.2/10.2
   10.1/10.1
   10.0/10.0
