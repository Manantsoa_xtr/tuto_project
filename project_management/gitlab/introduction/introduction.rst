.. index::
   pair: Introduction ; Gitlab

.. _gitlab_intro:

====================
Gitlab introduction
====================

.. seealso::

   - https://en.wikipedia.org/wiki/GitLab
   - https://about.gitlab.com/
   - https://twitter.com/sytses (Sid Sijbrandij, CEO de Gitlab)


GitLab is a web-based Git-repository manager with wiki and issue-tracking 
features, using an open-source license, developed by GitLab Inc.

The software was written by Dmitriy Zaporozhets. 
As of December 2016, the company has 150 team members and more than 
1400 open-source contributors. 
