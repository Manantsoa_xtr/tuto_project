.. index::
   pair: Project mangement ; Gitlab

.. _gitlab:

====================
Gitlab
====================

.. seealso::

   - https://en.wikipedia.org/wiki/GitLab
   - https://about.gitlab.com/
   - https://twitter.com/sytses (Sid Sijbrandij, CEO de Gitlab)
   - https://twitter.com/dzaporozhets (Gitlab cofounder, Dmitriy Zaporozhets)


.. figure:: GitLab_logo.png
   :align: center



.. toctree::
   :maxdepth: 3
   
   introduction/introduction
   versions/versions
